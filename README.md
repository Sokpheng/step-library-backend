# Getting Started

This project is about the STEP-E-Library exam from STEP.

This project uses Reactjs + Tailwindcss + DaisyUI as a UI-Library

# Available Scripts

## Install Dependencies

Run `yarn` or `yarn install`

**Note: Rename ".eve.example" to ".eve"**

## Run Command

Run `yarn dev`
Open `http://localhost:3000/`

# Login Test

## Teacher Role

username: `sokpheng`
password: `123456`

## Librarian Role

username: `sokpheng_librarian`
password: `123456`

# Done

Happy Project!!!
