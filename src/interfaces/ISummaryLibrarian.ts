export interface ISummaryLibrarian {
  books: string | undefined;
  groups: string | undefined;
  students: string | undefined;
  teachers: string | undefined;
  download: string | undefined;
}
