export interface IGroup {
  id: string | number;
  name: string;
}
