import { IGroup } from "./IGroup";

interface User {
  id: string | number;
  username: string;
  role: string;
  token: string;
  image: string | null;
}

export interface IUser extends User {
  gorup_id: string | null;
  pwd: string;
  remote_addr: string | null;
  forward_addr: string | null;
}

export interface IUserDetails extends User {
  groups: IGroup[];
}
