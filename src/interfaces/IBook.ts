export interface IBook {
  id: string | number;
  title: string;
  path: string | null;
  user_id: string | number | null;
  group_id: string | number | null;
}
