import { apiKey, apiUrl } from "../config";

export const useApi = () => {
  const get = (endpoint: string) =>
    fetch(apiUrl + endpoint)
      .then((resp) => resp.json())
      .then((resp) => {
        return resp;
      })
      .catch((err) => console.log(err));
  const post = (endpoint: string, data: any) => {
    const value = { api_token: apiKey, ...data };
    return fetch(apiUrl + endpoint, {
      method: "POST",
      body: JSON.stringify(value),
    })
      .then((resp) => resp.json())
      .then((resp) => {
        console.log("data returned: ", resp);
        return resp;
      })
      .catch((err) => console.log(err));
  };
  const remove = (endpoint: string, data: any) => {
    console.log("Delete: ", JSON.stringify(data));

    fetch(apiUrl + endpoint, {
      method: "DELETE",
      body: JSON.stringify(data),
    })
      .then((resp) => resp.json())
      .then((resp) => {
        return resp;
      })
      .catch((err) => console.log(err));
  };
  const update = (endpoint: string, id: string, data: any) =>
    fetch(apiKey + endpoint + "/" + id, {
      method: "PUT",
      body: { api_token: apiKey, ...data },
    })
      .then((resp) => resp.json())
      .then((resp) => {
        return resp;
      })
      .catch((err) => console.log(err));

  return { get, post, remove, update };
};
