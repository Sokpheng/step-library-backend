import React from "react";
import { IconType } from "react-icons/lib";

type Props = {
  value: string | undefined;
  title?: string;
  children: React.ReactNode;
  isLoading?: boolean;
};

function CardSummary({ title, value, children, isLoading }: Props) {
  return (
    <div className="card bg-slate-200 grow">
      <div className="card-body items-center text-center">
        {children}
        <h3 className="text-lg font-bold text-primary opacity-50">{title}</h3>
        {isLoading ? (
          <button className="btn loading btn-ghost disabled"></button>
        ) : (
          <h2 className="text-6xl font-bold mt-6">{value || 0}</h2>
        )}
      </div>
    </div>
  );
}

export default CardSummary;
