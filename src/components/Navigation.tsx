import { Link, useLocation, useNavigate } from "react-router-dom";
import { MdDashboard } from "react-icons/md";
import { IconType } from "react-icons/lib/esm/iconBase";
import { ReactNode, Fragment } from "react";
import { NavLink } from "react-router-dom";
import { ROLE } from "../constants";
import { useAppSelector } from "../hooks";

export interface menuOptions {
  name: string;
  icon?: ReactNode;
  path: string;
  exceptRole?: ROLE;
}

type Props = {
  menus: menuOptions[];
};

function Navigation({ menus }: Props) {
  const user = useAppSelector((state) => state.user);

  const userRole = user?.data?.role;

  return (
    <div className="flex flex-col grow overflow-auto whitespace-nowrap">
      <div className="p-6 sticky top-0 bg-gray-100 z-10">
        <h1 className="font-semibold text-2xl text-primary">STEP E-library</h1>
      </div>
      <div className="divider m-0 h-0"></div>
      <ul className="menu flex flex-col grow">
        {menus.map((menu, i) => (
          <Fragment key={menu.name + i}>
            {menu?.exceptRole != userRole && (
              <li className="whitespace-nowrap">
                <NavLink
                  to={menu.path}
                  className={({ isActive }) =>
                    isActive ? "active bordered" : ""
                  }
                >
                  <span>{menu?.icon}</span> {menu.name}
                </NavLink>
              </li>
            )}
          </Fragment>
        ))}
      </ul>
    </div>
  );
}

export default Navigation;
