import React from "react";
import { AiFillNotification } from "react-icons/ai";

type Props = {
  message: string;
};

function NotifyMessage({ message }: Props) {
  return (
    <div className="min-h-16 grow p-6">
      <div className="flex flex-col justify-center grow items-center space-y-12 text-error">
        <AiFillNotification className="text-4xl md:text-8xl text-error" />
        <p className="text-lg font-medium uppercase ">{message}</p>
      </div>
    </div>
  );
}

export default NotifyMessage;
