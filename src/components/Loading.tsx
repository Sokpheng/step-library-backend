import React from "react";

type Props = {};

function Loading({}: Props) {
  return (
    <div className="mt-12 flex flex-col justify-center items-center space-y-4">
      <h2>Loading...</h2>
      <progress className="progress w-56"></progress>
    </div>
  );
}

export default Loading;
