import React from "react";

type Props = {
  labelElement: React.ReactElement;
  id: string;
  children: React.ReactNode;
};

function Modal({ id, children, labelElement }: Props) {
  return (
    <div>
      {/* <!-- The button to open modal --> */}
      {labelElement}
      {/* <!-- Put this part before </body> tag --> */}
      <input type="checkbox" id={id} className="modal-toggle" />
      <div className="modal modal-bottom sm:modal-middle">
        <div className="modal-box">
          <label
            htmlFor={id}
            className="btn btn-sm btn-circle absolute right-2 top-2"
          >
            ✕
          </label>
          {children}
        </div>
      </div>
    </div>
  );
}

export default Modal;
