import React from "react";

type Props = {
  labelElement: React.ReactElement;
  id: string;
  children: React.ReactNode;
  showCancelLabelElement?: boolean;
  cancelLabelText?: string;
  confirmLabelElement: React.ReactElement;
};

function ModalConfirm({
  id,
  labelElement,
  children,
  confirmLabelElement: confirmBtn,
  cancelLabelText: cancelBtnText,
  showCancelLabelElement: showCancelBtn,
}: Props) {
  return (
    <div>
      {/* <!-- The button to open modal --> */}
      {labelElement}
      {/* <!-- Put this part before </body> tag --> */}
      <input type="checkbox" id={id} className="modal-toggle" />
      <div className="modal modal-bottom sm:modal-middle">
        <div className="modal-box">
          <label
            htmlFor={id}
            className="btn btn-sm btn-circle absolute right-2 top-2"
          >
            ✕
          </label>
          {children}
          <div className="flex justify-end space-x-2 items-center">
            {/* Show Cancel */}
            {showCancelBtn && (
              <div className="modal-action">
                <label htmlFor={id} className="btn">
                  {cancelBtnText || "Cancel"}
                </label>
              </div>
            )}
            {/* Confirm Button */}
            <div className="modal-action">{confirmBtn}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalConfirm;
