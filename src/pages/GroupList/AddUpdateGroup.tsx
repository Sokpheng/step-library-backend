import { useFormik } from "formik";
import React, { useEffect, useState } from "react";
import { BsPencilSquare } from "react-icons/bs";
import { toast } from "react-toastify";
import { object, string } from "yup";
import Modal from "../../components/modal/Modal";
import { apiKey } from "../../config";
import { useAppSelector } from "../../hooks";
import { useApi } from "../../hooks/useHttp";
import { IGroup } from "../../interfaces/IGroup";

type Props = {
  type: "add" | "update";
  groupItem?: IGroup;
};

interface ICreateGroup {
  name: string;
  api_token: string;
  user_token: string;
}

interface IUpdateGroup extends ICreateGroup {
  group_id: string | number;
}

function AddUpdateGroup({ type, groupItem }: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const user = useAppSelector((state) => state.user);
  const [groupProp, setGroupProp] = useState<IGroup | undefined>(groupItem);
  const api = useApi();
  const userToken = user?.data?.token;
  const [initialValues, setInitialValues] = useState<
    ICreateGroup | IUpdateGroup
  >({
    api_token: apiKey,
    user_token: userToken,
    name: "",
  });

  useEffect(() => {
    if (groupItem) {
      setInitialValues({
        ...initialValues,
        group_id: groupItem.id,
        name: groupItem.name,
      });
    }
  }, []);

  const validationSchema = object().shape({
    name: string().required(),
  });

  const formik = useFormik<ICreateGroup | IUpdateGroup>({
    initialValues,
    enableReinitialize: true,
    validationSchema,
    onSubmit: async (values) => {
      console.log("Values: ", values);
      const data = values;
      setIsLoading(true);
      await api.post("/addorupdategroup.php", data).then((resp) => {
        if (resp.status === "SUCCESS") {
          type === "add"
            ? toast.success("Group added successfully.")
            : toast.success("Group update successfully.");
        } else {
          toast.error(resp.error);
        }
      });
      setIsLoading(false);
    },
  });

  return (
    <Modal
      id={type === "add" ? "modal-create" : "modal-update"}
      labelElement={
        type == "add" ? (
          <label
            htmlFor={"modal-create"}
            className="btn btn-primary btn-outline modal-button"
          >
            Add New Group
          </label>
        ) : (
          <label
            htmlFor={"modal-update"}
            className="btn btn-square btn-outline btn-warning btn-sm"
          >
            <BsPencilSquare />
          </label>
        )
      }
    >
      <div className="w-full flex justify-start">
        <h2 className="text-lg font-semibold uppercase">
          {type === "add" ? "Add New Group" : "Update Group"}
        </h2>
      </div>
      <div className="flex flex-col space-y-4 mt-6">
        <form onSubmit={formik.handleSubmit}>
          <div>
            <input
              name="name"
              type="text"
              value={formik.values.name}
              placeholder="Name"
              className="input input-bordered w-full input-primary"
              onChange={formik.handleChange}
            />
            {formik.touched.name && formik.errors.name ? (
              <p className="text-error">{formik.errors.name}</p>
            ) : null}
          </div>
          {/* Action buttons */}
          <div className="flex justify-end items-center space-x-2 mt-6">
            <label
              htmlFor={type === "add" ? "modal-create" : "modal-update"}
              className="btn btn-ghost btn-outline"
            >
              Cancel
            </label>
            {type === "add" ? (
              <button
                type="submit"
                className={`btn btn-outline btn-primary ${
                  isLoading && "loading"
                }`}
              >
                Create Group
              </button>
            ) : (
              <button
                type="submit"
                className={`btn btn-outline btn-primary ${
                  isLoading && "loading"
                }`}
              >
                Save Changes
              </button>
            )}
          </div>
        </form>
      </div>
    </Modal>
  );
}

export default AddUpdateGroup;
