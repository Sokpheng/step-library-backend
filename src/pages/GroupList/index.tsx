import { useState, useEffect, Fragment } from "react";
import { AiFillEye } from "react-icons/ai";
import { MdOutlineDeleteForever } from "react-icons/md";
import { NavLink } from "react-router-dom";
import { toast } from "react-toastify";
import Loading from "../../components/Loading";
import ModalConfirm from "../../components/modal/ModalConfirm";
import NotifyMessage from "../../components/NotifyMessage";
import { apiKey } from "../../config";
import { ROLE } from "../../constants";
import { useAppSelector } from "../../hooks";
import { useApi } from "../../hooks/useHttp";
import { IGroup } from "../../interfaces/IGroup";
import AddUpdateGroup from "./AddUpdateGroup";

const GroupList = () => {
  const [groupList, setGroupList] = useState<IGroup[]>([]);
  const [isLoadingGroup, setIsLoadingGroup] = useState<boolean>(true);
  const api = useApi();
  const user = useAppSelector((state) => state.user);
  const userToken = user?.data?.token;

  useEffect(() => {
    console.log("callllll");
    api
      .get(`/groups.php?api_token=${apiKey}&user_token=${userToken}`)
      .then((resp) => {
        console.log("respss: ", resp);
        setIsLoadingGroup(false);
        setGroupList(resp);
      });
  }, []);

  const onDeleteGroup = (groupId: string | number) => {
    api
      .post("/removegroup.php", {
        group_id: groupId,
        api_token: apiKey,
        user_token: userToken,
      })
      .then((resp) => {
        if (resp.status === "SUCCESS") {
          toast.success("Group deleted successfully.");
          const filterGroup = groupList.filter((group) => group.id != groupId);
          setGroupList(filterGroup);
        } else {
          toast.error(resp.error);
        }
      });
  };

  return (
    <div className="container mx-auto flex flex-col p-6">
      <div className="flex justify-between items-end">
        <h1 className="uppercase text-2xl font-bold text-center text-primary">
          Group List
        </h1>
        {user?.data?.role === ROLE.LIBRARIAN && <AddUpdateGroup type="add" />}
      </div>
      {/* Table groups */}
      <div className="overflow-x-auto mt-6 ">
        <table className="table w-full">
          {/* <!-- head --> */}
          <thead>
            <tr>
              <th>ID</th>
              <th className="w-full">Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {/* <!-- row --> */}
            {groupList.map((group) => (
              <tr key={group.id}>
                <th className="bg-gray-50">{group.id}</th>
                <td className="bg-gray-50">{group.name}</td>
                <td className="bg-gray-50 space-x-1">
                  <div className="tooltip" data-tip="View Details">
                    <NavLink
                      to={`/group-list/${group.id}`}
                      className="btn btn-sm btn-primary btn-outline"
                    >
                      <AiFillEye />
                    </NavLink>
                  </div>
                  {user?.data?.role === ROLE.LIBRARIAN && (
                    <Fragment>
                      {/* Update Book */}
                      <div className="tooltip" data-tip="Update">
                        <AddUpdateGroup type="update" groupItem={group} />
                      </div>
                      {/* Delete Book */}
                      <div className="tooltip" data-tip="Delete">
                        <ModalConfirm
                          id="modal-delete"
                          confirmLabelElement={
                            <label
                              htmlFor="modal-delete"
                              className="btn btn-error"
                              onClick={() => onDeleteGroup(group.id)}
                            >
                              Confirm
                            </label>
                          }
                          showCancelLabelElement
                          labelElement={
                            <label
                              htmlFor="modal-delete"
                              className="btn btn-square btn-outline btn-error btn-sm"
                            >
                              <MdOutlineDeleteForever />
                            </label>
                          }
                        >
                          <div className="flex flex-col justify-start space-y-6">
                            <div className="w-full text-left">
                              <h2 className="text-lg font-bold">
                                This group will be permanently delete!
                              </h2>
                            </div>
                            <p>Click corfirm to delete.</p>
                          </div>
                        </ModalConfirm>
                      </div>
                    </Fragment>
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {isLoadingGroup ? (
        <Loading />
      ) : (
        !groupList.length && <NotifyMessage message="No group available!" />
      )}
    </div>
  );
};

export default GroupList;
