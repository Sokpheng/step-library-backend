import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { useParams } from "react-router-dom";
import { apiKey } from "../../config";
import { useAppSelector } from "../../hooks";
import { useApi } from "../../hooks/useHttp";
import { AiFillNotification } from "react-icons/ai";
import NotifyMessage from "../../components/NotifyMessage";
import Loading from "../../components/Loading";

type Props = {};
interface IStudent {
  id: string | number;
  username: string;
  pwd: string;
  role: string;
  token: string | null;
  group_id: string | number | null;
  remote_addr: string | null;
  forward_addr: string | null;
}

function StudentInGroup({}: Props) {
  const [isLoading, setIsLoading] = useState(true);
  const [studentList, setStudentList] = useState<IStudent[]>([]);

  const param = useParams();
  const api = useApi();

  const user = useAppSelector((state) => state.user);
  const userToken = user?.data?.token;
  useEffect(() => {
    api
      .post("/students.php", {
        api_token: apiKey,
        group_id: param.id,
        user_token: userToken,
      })
      .then((resp) => {
        setStudentList(resp.students);
        setIsLoading(false);
      });
  }, []);

  return (
    <div className="container mx-auto flex flex-col p-6">
      <h1 className="uppercase text-2xl font-bold text-center text-primary">
        Group ID: {param.id}
      </h1>
      {/* Table Students */}
      <div className="overflow-x-auto mt-6 ">
        <table className="table w-full">
          {/* <!-- head --> */}
          <thead>
            <tr>
              <th>ID</th>
              <th className="w-full">Username</th>
              <th className="max-w-[200px] truncate">Password</th>
              <th>Role</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {/* <!-- row --> */}
            {studentList.map((student) => (
              <tr key={student.id}>
                <th className="bg-gray-50">{student.id}</th>
                <td className="bg-gray-50">{student.username}</td>
                <td className="bg-gray-50">{student.pwd}</td>
                <td className="bg-gray-50">{student.role}</td>
                <td className="bg-gray-50">
                  <NavLink
                    to={`/group-list/${student.id}`}
                    className="btn btn-sm btn-primary btn-outline"
                  >
                    Detail
                  </NavLink>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {isLoading ? (
          <Loading />
        ) : (
          !studentList.length && (
            <NotifyMessage message="This group doesn't have any students yet!" />
          )
        )}
      </div>
    </div>
  );
}

export default StudentInGroup;
