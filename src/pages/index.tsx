import React from "react";
import { NavLink } from "react-router-dom";
import { useAppSelector } from "../hooks";

function Home() {
  const user = useAppSelector((state) => state.user);

  if (!user?.data?.role)
    return (
      <div className="flex flex-col justify-center items-center grow min-h-screen space-y-6">
        <h1 className="text-2xl font-medium">Please login!</h1>
        <NavLink to="/auth" className="btn btn-primary">
          Go To Login
        </NavLink>
      </div>
    );
  else
    return (
      <div className="flex flex-col justify-center items-center grow min-h-screen space-y-6">
        {/* <h1 className="text-2xl font-medium"></h1> */}
        <NavLink to="/dashboard" className="btn btn-primary">
          Go To Dashboard
        </NavLink>
      </div>
    );
}

export default Home;
