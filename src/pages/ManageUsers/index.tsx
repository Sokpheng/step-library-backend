import { useState, useEffect } from "react";
import { BsPencilSquare } from "react-icons/bs";
import { ImDownload2 } from "react-icons/im";
import { MdOutlineDeleteForever } from "react-icons/md";
import { NavLink } from "react-router-dom";
import { toast } from "react-toastify";
import Loading from "../../components/Loading";
import Modal from "../../components/modal/Modal";
import ModalConfirm from "../../components/modal/ModalConfirm";
import NotifyMessage from "../../components/NotifyMessage";
import { apiKey } from "../../config";
import { ROLE } from "../../constants";
import { useAppSelector } from "../../hooks";
import { useApi } from "../../hooks/useHttp";
import { IUser } from "../../interfaces/IUser";
import AddUserUserForm from "./components/AddUpdateUserForm";
import UpdatePasswordForm from "./components/UpdatePasswordForm";

type Props = {};

function ManageUsers({}: Props) {
  const [isLoadingUser, setIsLoadingUser] = useState(true);
  const [userList, setUserList] = useState<IUser[]>([]);
  const [search, setSearch] = useState<string>("");
  const [groupId, setGroupId] = useState<string | number>("");
  const [role, setRole] = useState<ROLE>();
  const [sortOrder, SetsortOrder] = useState<"asc" | "desc">("asc");
  const userStore = useAppSelector((state) => state.user);
  const userToken = userStore?.data?.token;
  const api = useApi();

  useEffect(() => {
    getUserList();
  }, []);

  const getUserList = () => {
    setIsLoadingUser(true);
    api
      .post("/users.php", {
        api_token: apiKey,
        user_token: userToken,
        // search: search,
        // group_id: groupId,
        // role,
        // sort_order: sortOrder,
      })
      .then((resp) => {
        setUserList(resp.users);
        setIsLoadingUser(false);
      });
  };

  const onDeleteUser = (userId: string | number) => {
    api
      .post("/removeuser.php", {
        user_id: userId,
        api_token: apiKey,
        user_token: userToken,
      })
      .then((resp) => {
        if (resp.status === "SUCCESS") {
          toast.success("User deleted successfully.");
          const filterUser = userList.filter((user) => user.id != userId);
          setUserList(filterUser);
        } else {
          toast.error(resp.error);
        }
      });
  };

  return (
    <div className="container mx-auto flex flex-col p-6">
      <div className="flex justify-between items-end">
        <h1 className="uppercase text-2xl font-bold text-center text-primary">
          Manage Users
        </h1>
        {userStore?.data?.role === ROLE.LIBRARIAN && (
          <AddUserUserForm type="add" />
        )}
      </div>
      {/* Table groups */}
      <div className="overflow-x-auto mt-6 ">
        <table className="table w-full">
          {/* <!-- head --> */}
          <thead>
            <tr>
              <th>ID</th>
              <th className="w-full">Username</th>
              <th className="max-w-xs truncate whitespace-nowrap">Password</th>
              <th className="max-w-xs">Group ID</th>
              <th className="max-w-xs">Role</th>
              {userStore?.data?.role === ROLE.LIBRARIAN && <th>Actions</th>}
            </tr>
          </thead>
          <tbody>
            {/* <!-- row --> */}
            {userList.map((user) => (
              <tr key={user.id}>
                <th className="bg-gray-50">{user.id}</th>
                <td className="bg-gray-50">{user.username}</td>
                <td className="bg-gray-50">{user.pwd}</td>
                <td className="bg-gray-50">{user.gorup_id}</td>
                <td className="bg-gray-50">{user.role}</td>
                {userStore?.data?.role === ROLE.LIBRARIAN && (
                  <td className="bg-gray-50 space-x-1">
                    {/* Update Password */}
                    <div className="tooltip" data-tip="Update Password">
                      <UpdatePasswordForm userId={user.id} />
                    </div>
                    {/* Update Book */}
                    <div className="tooltip" data-tip="Update">
                      <AddUserUserForm type="update" userData={user} />
                    </div>
                    {/* Delete Book */}
                    <div className="tooltip" data-tip="Delete">
                      <ModalConfirm
                        id="modal-delete"
                        confirmLabelElement={
                          <label
                            htmlFor="modal-delete"
                            className="btn btn-error"
                            onClick={() => onDeleteUser(user.id)}
                          >
                            Confirm
                          </label>
                        }
                        showCancelLabelElement
                        labelElement={
                          <label
                            htmlFor="modal-delete"
                            className="btn btn-square btn-outline btn-error btn-sm"
                          >
                            <MdOutlineDeleteForever />
                          </label>
                        }
                      >
                        <div className="flex flex-col justify-start space-y-6">
                          <div className="w-full text-left">
                            <h2 className="text-lg font-bold">
                              This user will be permanently delete!
                            </h2>
                          </div>
                          <p>Click corfirm to delete.</p>
                        </div>
                      </ModalConfirm>
                    </div>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {isLoadingUser ? (
        <Loading />
      ) : (
        !userList.length && <NotifyMessage message="No User yet!" />
      )}
    </div>
  );
}

export default ManageUsers;
