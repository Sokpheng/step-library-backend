import { useFormik } from "formik";
import React, { useEffect, useState, Fragment } from "react";
import { BsPencilSquare } from "react-icons/bs";
import { toast } from "react-toastify";
import { object, string } from "yup";
import Modal from "../../../components/modal/Modal";
import { apiKey } from "../../../config";
import { ROLE } from "../../../constants";
import { useAppSelector } from "../../../hooks";
import { useApi } from "../../../hooks/useHttp";
import { IGroup } from "../../../interfaces/IGroup";
import { IUser } from "../../../interfaces/IUser";

type Props = {
  type: "add" | "update";
  userData?: IUser;
};

interface ICreateUser {
  api_token: string;
  user_token: string;
  username: string;
  pwd: string;
  confirm_pwd: string;
  role: string;
  group_id: string | number | null;
}

interface IUpdateUser {
  user_id: string | number;
  api_token: string;
  user_token: string;
  username: string;
  role: string;
  group_id: string | number | null;
}

interface MapGroup {
  id: string;
  label: string;
}

function AddUserUserForm({ type, userData }: Props) {
  const user = useAppSelector((state) => state.user);
  const userToken = user?.data?.token;
  const [groupList, setGroupList] = useState<MapGroup[]>([]);
  const [initialValues, setinitialValues] = useState<ICreateUser>({
    api_token: apiKey,
    user_token: userToken,
    group_id: null,
    pwd: "",
    confirm_pwd: "",
    username: "",
    role: ROLE.STUDENT,
  });

  const [initialValuesUpdate, setinitialValuesUpdate] = useState<IUpdateUser>({
    api_token: apiKey,
    user_token: userToken,
    user_id: "",
    group_id: "",
    role: "",
    username: "",
  });

  const api = useApi();

  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    if (userData) {
      setinitialValuesUpdate({
        api_token: apiKey,
        user_token: userToken,
        user_id: userData.id,
        group_id: userData.id,
        role: userData.role,
        username: userData.username,
      });
    }
    api
      .get(`/groups.php?api_token=${apiKey}&user_token=${userToken}`)
      .then((resp) => {
        if (resp) {
          const mapGroup = resp.map((group: IGroup) => {
            return {
              id: group.id,
              label: group.name,
            };
          });
          setGroupList(mapGroup);
        }
      });
  }, []);

  const validationSchemaAdd = object().shape({
    api_token: string().required(),
    user_token: string().required(),
    pwd: string().required(),
    confirm_pwd: string().required(),
    username: string().required(),
    role: string().required(),
  });

  const validationSchemaUpdate = object().shape({
    api_token: string().required(),
    user_token: string().required(),
    username: string().required(),
    role: string().required(),
  });

  const formik = useFormik<ICreateUser | IUpdateUser>({
    initialValues: type === "add" ? initialValues : initialValuesUpdate,
    enableReinitialize: true,
    validationSchema:
      type === "add" ? validationSchemaAdd : validationSchemaUpdate,
    onSubmit: async (values) => {
      const data = values;
      console.log("data: ", data);
      setIsLoading(true);
      await api.post("/addorupdateuser.php", values).then((resp) => {
        if (resp.status == "SUCCESS") {
          type == "add"
            ? toast.success("User create successfully.")
            : toast.success("User update successfully.");
        } else {
          toast.error(resp.error);
        }
      });
      setIsLoading(false);
    },
  });
  return (
    <Modal
      id={type === "add" ? "modal-create" : "modal-update"}
      labelElement={
        type == "add" ? (
          <label
            htmlFor={type === "add" ? "modal-create" : "modal-update"}
            className="btn btn-primary btn-outline modal-button"
          >
            Add New User
          </label>
        ) : (
          <label
            htmlFor="modal-update"
            className="btn btn-square btn-outline btn-error btn-sm"
          >
            <BsPencilSquare />
          </label>
        )
      }
    >
      <div className="flex flex-col justify-start">
        <h2 className="text-lg font-semibold uppercase">
          {type === "add" ? "Add New User" : "Update User"}
        </h2>
        {/* Form */}
        <form onSubmit={formik.handleSubmit}>
          <div className="flex flex-col space-y-4 mt-6">
            <div>
              <input
                name="username"
                type="text"
                defaultValue={formik.values.username}
                placeholder="Username"
                className="input input-bordered w-full input-primary"
                onChange={formik.handleChange}
              />
              {formik.touched.username && formik.errors.username ? (
                <p className="text-error">{formik.errors.username}</p>
              ) : null}
            </div>
            {type === "add" ? (
              <Fragment>
                <div>
                  <input
                    name="pwd"
                    type="text"
                    placeholder="Password"
                    className="input input-bordered input-primary w-full"
                    onChange={formik.handleChange}
                  />
                  {/* {formik.touched.pwd && formik.errors.pwd ? (
                    <p className="text-error">{formik.errors.pwd}</p>
                  ) : null} */}
                </div>
                <div>
                  <input
                    name="confirm_pwd"
                    type="text"
                    placeholder="Confirm Password"
                    className="input input-bordered input-primary w-full"
                    onChange={formik.handleChange}
                  />
                  {/* {formik.touched.confirm_pwd && formik.errors.confirm_pwd ? (
                    <p className="text-error">{formik.errors.confirm_pwd}</p>
                  ) : null} */}
                </div>
              </Fragment>
            ) : null}
            <div>
              <select
                placeholder="Role"
                defaultValue={formik.values.role}
                name="role"
                className="select select-bordered select-primary w-full"
                onChange={formik.handleChange}
              >
                <option defaultValue={ROLE.STUDENT}>{ROLE.STUDENT}</option>
                <option value={ROLE.LIBRARIAN}>{ROLE.LIBRARIAN}</option>
                <option value={ROLE.TEACHER}>{ROLE.TEACHER}</option>
              </select>
              {formik.touched.role && formik.errors.role ? (
                <p className="text-error">{formik.errors.role}</p>
              ) : null}
            </div>
            <select
              placeholder="Group"
              name="group_id"
              defaultValue={formik.values.group_id || undefined}
              className="select select-bordered select-primary w-full"
              onChange={formik.handleChange}
            >
              {groupList.length ? (
                groupList.map((group) => (
                  <option key={group.id} value={group.id}>
                    {group.label}
                  </option>
                ))
              ) : (
                <option disabled defaultValue={undefined}>
                  No group available
                </option>
              )}
            </select>
          </div>
          {/* Action buttons */}
          <div className="flex justify-end items-center space-x-2 mt-6">
            <label
              htmlFor={type === "add" ? "modal-create" : "modal-update"}
              className="btn btn-ghost btn-outline"
            >
              Cancel
            </label>
            {type === "add" ? (
              <button
                type="submit"
                className={`btn btn-outline btn-primary ${
                  isLoading && "loading"
                }`}
              >
                Create User
              </button>
            ) : (
              <button
                type="submit"
                className={`btn btn-outline btn-primary ${
                  isLoading && "loading"
                }`}
              >
                Save Changes
              </button>
            )}
          </div>
        </form>
      </div>
    </Modal>
  );
}

export default AddUserUserForm;
