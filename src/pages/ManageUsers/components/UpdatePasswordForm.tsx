import { useFormik } from "formik";
import { useState } from "react";
import { BsFillKeyFill } from "react-icons/bs";
import { object, string } from "yup";
import Modal from "../../../components/modal/Modal";
import { useAppSelector } from "../../../hooks";
import { useApi } from "../../../hooks/useHttp";

type Props = {
  userId: string | number;
};

interface IUpdatePassword {
  pwd: string;
  old_pwd: string;
  confirm_pwd: string;
}

function UpdatePasswordForm({ userId }: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const user = useAppSelector((state) => state.user);
  const userToken = user?.data?.token;

  const api = useApi();

  const validationSchema = object().shape({
    pwd: string().required(),
    confirm_pwd: string().required(),
    old_pwd: string().required(),
  });

  const formik = useFormik<IUpdatePassword>({
    initialValues: {
      pwd: "",
      confirm_pwd: "",
      old_pwd: "",
    },
    validationSchema,
    onSubmit: async (values) => {
      const data = values;
      console.log("data: ", data);
    },
  });

  return (
    <Modal
      id="model-update-password"
      labelElement={
        <label
          htmlFor="model-update-password"
          className="btn btn-square btn-outline btn-error btn-sm"
        >
          <BsFillKeyFill />
        </label>
      }
    >
      <div className="flex flex-col justify-start text-start">
        <h2 className="text-lg font-semibold uppercase">Update Password</h2>
        {/* Form */}
        <form onSubmit={formik.handleSubmit}>
          <div className="flex flex-col  space-y-4 mt-6">
            <div>
              <input
                name="old_pwd"
                type="text"
                placeholder="Old Password"
                className="input input-bordered input-primary w-full"
                onChange={formik.handleChange}
              />
              {formik.touched.old_pwd && formik.errors.old_pwd ? (
                <p className="text-error">{formik.errors.old_pwd}</p>
              ) : null}
            </div>
            <div>
              <input
                name="pwd"
                type="text"
                placeholder="New Password"
                className="input input-bordered w-full input-primary"
                onChange={formik.handleChange}
              />
              {formik.touched.pwd && formik.errors.pwd ? (
                <p className="text-error">{formik.errors.pwd}</p>
              ) : null}
            </div>
            <div>
              <input
                name="confirm_pwd"
                type="text"
                placeholder="Confirm Password"
                className="input input-bordered input-primary w-full"
                onChange={formik.handleChange}
              />
              {formik.touched.confirm_pwd && formik.errors.confirm_pwd ? (
                <p className="text-error">{formik.errors.confirm_pwd}</p>
              ) : null}
            </div>
          </div>
          <div className="flex justify-end items-center space-x-2 mt-6">
            <label
              htmlFor="model-update-password"
              className="btn btn-ghost btn-outline"
            >
              Cancel
            </label>
            <button
              type="submit"
              className={`btn btn-outline btn-primary ${
                isLoading && "loading"
              }`}
            >
              Save Changes
            </button>
          </div>
        </form>
      </div>
    </Modal>
  );
}

export default UpdatePasswordForm;
