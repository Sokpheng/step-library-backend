import React from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { useAppSelector } from "../hooks";
import { ERole } from "../interfaces/IRole";

function NotFoundPage() {
  const user = useAppSelector((state) => state.user);
  const navigate = useNavigate();
  const backHome = () => {
    navigate("/");
  };
  return (
    <div className="flex flex-col justify-center items-center grow min-h-screen space-y-6">
      <h1 className="text-2xl font-semibold uppercase text-error">
        404 Not Found
      </h1>
      <button className="btn btn-primary btn-wide" onClick={backHome}>
        Back Home
      </button>
    </div>
  );
}

export default NotFoundPage;
