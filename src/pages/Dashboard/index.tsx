import { useAppSelector } from "../../hooks";
import { ROLE } from "../../constants";
import DashboardTeacher from "./DashboardTeacher";
import DashboardLibrarian from "./DashboardLibrarian";

function Dashboard() {
  const user = useAppSelector((state) => state.user);

  return user?.data?.role == ROLE.TEACHER ? (
    <DashboardTeacher />
  ) : (
    <DashboardLibrarian />
  );
}

export default Dashboard;
