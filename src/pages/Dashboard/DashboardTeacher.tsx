import { useEffect, useState, useMemo } from "react";
import { apiKey } from "../../config";
import { useAppSelector } from "../../hooks";
import { useApi } from "../../hooks/useHttp";
import { HiUserGroup } from "react-icons/hi";
import { SiBookstack } from "react-icons/si";
import { ImBooks } from "react-icons/im";
import { NavLink } from "react-router-dom";
import { MdAccountCircle } from "react-icons/md";
import { ROLE } from "../../constants";
import CardSummary from "../../components/card/CardSummary";
type Props = {};

function DashboardTeacher({}: Props) {
  const [isLoading, setIsLoading] = useState(true);
  const [summary, setSummary] = useState<{
    books: string;
    groups: string;
  } | null>(null);
  const api = useApi();
  const user = useAppSelector((state) => state.user);

  useEffect(() => {
    api
      .post("/summary.php", {
        api_token: apiKey,
        user_token: user?.data?.token,
      })
      .then((resp) => {
        console.log("resppppp ", resp);
        setSummary(resp?.summary);
        setIsLoading(false);
      });
  }, []);

  return (
    <div className="flex flex-col grow">
      <div className="container mx-auto">
        {/* Summary */}
        <div className="w-fit mt-6 text-primary px-6">
          <h1 className="text-3xl font-bold uppercase">Summary</h1>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-6 p-6 w-full items-center">
          <CardSummary
            isLoading={isLoading}
            value={summary?.books}
            title="Books"
          >
            <ImBooks className="text-6xl text-primary opacity-30" />
          </CardSummary>
          <CardSummary
            isLoading={isLoading}
            value={summary?.groups}
            title="Groups"
          >
            <HiUserGroup className="text-6xl text-primary opacity-30" />
          </CardSummary>
        </div>
        {/* Link */}
        <div className="w-fit mt-6 text-primary px-6">
          <h1 className="text-3xl font-bold uppercase">Links</h1>
        </div>
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 p-6 w-full justify-start items-center">
          <div className="card bg-primary border hover:bg-opacity-10 transition-colors duration-300 bg-opacity-0">
            <NavLink to="/group-list">
              <span className="card-body flex flex-col items-center">
                <HiUserGroup className="text-4xl text-primary opacity-30" />
                <p className="text-xl mt-2">Group List</p>
              </span>
            </NavLink>
          </div>
          <div className="card bg-primary border hover:bg-opacity-10 transition-colors duration-300 bg-opacity-0">
            <NavLink to="/book-list">
              <span className="card-body flex flex-col items-center">
                <SiBookstack className="text-4xl text-primary opacity-30" />
                <p className="text-xl mt-2">Book List</p>
              </span>
            </NavLink>
          </div>
          <div className="card bg-primary border hover:bg-opacity-10 transition-colors duration-300 bg-opacity-0">
            <NavLink to="/account">
              <span className="card-body flex flex-col items-center">
                <MdAccountCircle className="text-4xl text-primary opacity-30" />
                <p className="text-xl mt-2">Profile</p>
              </span>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DashboardTeacher;
