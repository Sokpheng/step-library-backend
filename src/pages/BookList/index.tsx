import { useState, useEffect } from "react";
import { AiFillNotification } from "react-icons/ai";
import { NavLink } from "react-router-dom";
import { ROLE } from "../../constants";
import { useAppSelector } from "../../hooks";
import { useApi } from "../../hooks/useHttp";
import { BsDownload, BsPencilSquare } from "react-icons/bs";
import { MdOutlineDeleteForever } from "react-icons/md";
import { apiKey, apiUrl } from "../../config";
import NotifyMessage from "../../components/NotifyMessage";
import { ImDownload2 } from "react-icons/im";
import ModalConfirm from "../../components/modal/ModalConfirm";
import { IBook } from "../../interfaces/IBook";
import Loading from "../../components/Loading";

type Props = {};

function BookList({}: Props) {
  const [isLoading, setIsLoading] = useState(true);
  const [bookList, setbookList] = useState<IBook[]>([]);
  const [search, setSearch] = useState<string>("");
  const [sort, setSort] = useState<"asc" | "desc">("asc");
  const user = useAppSelector((state) => state.user);
  const userToken = user?.data?.token;
  const group_id = user?.data?.group_id;
  const api = useApi();

  useEffect(() => {
    user?.data?.group_id &&
      api
        .post("/books.php", {
          api_token: apiKey,
          user_token: userToken,
          group_id,
          search,
          sort,
        })
        .then((resp) => {
          setbookList(resp.books);
          setIsLoading(false);
        });
  }, [search, sort]);

  if (user?.data?.role != ROLE.TEACHER)
    return (
      <div className="h-full flex items-center">
        <NotifyMessage message="Only available For Teacher!" />;
      </div>
    );

  if (!user?.data?.group_id)
    return (
      <div className="h-full flex items-center">
        <NotifyMessage message="Your account doesn't have any group yet!" />;
      </div>
    );

  return (
    <div className="container p-6 mx-auto flex flex-col">
      <div className="flex justify-between items-end">
        <h1 className="uppercase text-2xl font-bold text-center text-primary">
          Books
        </h1>
        <ModalConfirm
          id="modal-create"
          confirmLabelElement={
            <label htmlFor="modal-create" className="btn btn-error">
              Create
            </label>
          }
          showCancelLabelElement
          labelElement={
            <label
              htmlFor="modal-create"
              className="btn btn-primary btn-outline modal-button"
            >
              Add New Book
            </label>
          }
        >
          <p>Update form here....</p>
        </ModalConfirm>
      </div>

      {/* Table Books */}
      <div className="overflow-x-auto mt-6 ">
        <table className="table w-full">
          {/* <!-- head --> */}
          <thead>
            <tr>
              <th>ID</th>
              <th className="w-full">Title</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {/* <!-- row --> */}
            {bookList.map((book) => (
              <tr key={book.id}>
                <th className="bg-gray-50">{book.id}</th>
                <td className="bg-gray-50">{book.title}</td>
                <td className="bg-gray-50 flex items-center space-x-1">
                  <div data-tip="Download this book." className="tooltip">
                    <a
                      href={`http://172.104.166.110/${book.path}?api_token=${apiKey}&user_toke=${userToken}`}
                      target="_blank"
                      className="btn btn-square btn-primary btn-sm"
                    >
                      <ImDownload2 />
                    </a>
                  </div>

                  {/* Update Book */}
                  <div className="tooltip" data-tip="Update">
                    <ModalConfirm
                      id="modal-update"
                      confirmLabelElement={
                        <label htmlFor="modal-update" className="btn btn-error">
                          Save Changes
                        </label>
                      }
                      showCancelLabelElement
                      labelElement={
                        <label
                          htmlFor="modal-update"
                          className="btn btn-square btn-outline btn-warning btn-sm modal-button"
                        >
                          <BsPencilSquare />
                        </label>
                      }
                    >
                      <p>Update form here....</p>
                    </ModalConfirm>
                  </div>
                  {/* Delete Book */}
                  <div className="tooltip" data-tip="Delete">
                    <ModalConfirm
                      id="modal-delete"
                      confirmLabelElement={
                        <label htmlFor="modal-delete" className="btn btn-error">
                          Confirm
                        </label>
                      }
                      showCancelLabelElement
                      labelElement={
                        <label
                          htmlFor="modal-delete"
                          className="btn btn-square btn-outline btn-error btn-sm"
                        >
                          <MdOutlineDeleteForever />
                        </label>
                      }
                    >
                      <div className="flex justify-start space-y-6">
                        <h2 className="text-lg font-bold">
                          This book will be permanently delete!
                        </h2>
                        <p>Click corfirm to delete.</p>
                      </div>
                    </ModalConfirm>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {isLoading ? (
          <Loading />
        ) : (
          !bookList.length && <NotifyMessage message=" No book available!" />
        )}
      </div>
    </div>
  );
}

export default BookList;
