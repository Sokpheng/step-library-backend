import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import Loading from "../../components/Loading";
import NotifyMessage from "../../components/NotifyMessage";
import { apiKey } from "../../config";
import { ROLE } from "../../constants";
import { useAppSelector } from "../../hooks";
import { useApi } from "../../hooks/useHttp";
import { IUserDetails } from "../../interfaces/IUser";

type Props = {};

function Account({}: Props) {
  const [isLoading, setIsLoading] = useState(true);
  const [account, setAccount] = useState<IUserDetails>();
  const user = useAppSelector((state) => state.user);
  const userToken = user?.data?.token;
  const api = useApi();

  useEffect(() => {
    api
      .post("/profile.php", {
        api_token: apiKey,
        user_token: userToken,
      })
      .then((resp) => {
        setAccount(resp?.user);
        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return (
      <div className="h-full flex flex-col justify-center items-center">
        <Loading />
      </div>
    );
  }

  return (
    <div className="container mx-auto flex flex-col justify-center p-6">
      <div className="flex justify-between items-start w-full p-6 bg-primary bg-opacity-5 rounded-xl">
        {/* Info */}
        <div className="space-y-2 ">
          <h3>
            Username:
            <span className="text-xl font-bold ml-4">@{account?.username}</span>
          </h3>
          <p>
            Role:
            <span className="text-xl font-bold ml-4">{account?.role}</span>
          </p>
        </div>
        {/* Profile Picture */}
        <div className="avatar">
          <div className="w-36 border border-primary rounded-xl">
            <img src={account?.image || "static/images/no-profile.png"} />
          </div>
        </div>
      </div>
      {/* Groups Lists */}
      {user?.data?.role === ROLE.TEACHER && (
        <div className="mx-auto flex flex-col container mt-12">
          <h1 className="uppercase text-2xl font-bold text-center text-primary">
            Response Groups
          </h1>
          {/* Table groups */}
          <div className="overflow-x-auto mt-6 ">
            <table className="table w-full">
              {/* <!-- head --> */}
              <thead>
                <tr>
                  <th>ID</th>
                  <th className="w-full">Name</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {/* <!-- row --> */}
                {account?.groups.map((group) => (
                  <tr key={group.id}>
                    <th className="bg-gray-50">{group.id}</th>
                    <td className="bg-gray-50">{group.name}</td>
                    <td className="bg-gray-50">
                      <NavLink
                        to={`/group-list/${group.id}`}
                        className="btn btn-sm btn-primary btn-outline"
                      >
                        Detail
                      </NavLink>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            {!account?.groups.length && (
              <NotifyMessage message="You don't have any response group yet!" />
            )}
          </div>
        </div>
      )}
    </div>
  );
}

export default Account;
