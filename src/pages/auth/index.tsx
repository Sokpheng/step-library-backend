import axios from "axios";
import { useFormik } from "formik";
import { object, setLocale, string } from "yup";
import { useAppDispatch } from "../../hooks";
import { login } from "../../store/user/userSlice";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { ERole } from "../../interfaces/IRole";
import { useState } from "react";

function Login() {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  interface loginValue {
    username: string;
    password: string;
  }

  const validationSchema = object().shape({
    password: string().required(),
    username: string().required(),
  });

  const formik = useFormik<loginValue>({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema,
    onSubmit: async (values) => {
      setIsLoading(true);
      const data = values;
      const resp = await dispatch(login(data));
      if (resp?.status == "SUCCESS") {
        toast.success("Login successful");
        navigate("/");
      } else {
        toast.error(resp.error);
      }
      setIsLoading(false);
    },
  });

  return (
    <div className="bg-gray-200 min-h-screen flex justify-center items-center">
      <div className="p-12 flex flex-col rounded-lg space-y-6 shadow-lg bg-white">
        <h1 className="text-2xl font-bold uppercase">Login</h1>
        <div>
          <form onSubmit={formik.handleSubmit} className="space-y-6">
            <div className="space-y-4">
              <div>
                <label htmlFor="username">Username</label>
                <input
                  name="username"
                  onChange={formik.handleChange}
                  type="text"
                  placeholder="username"
                  className="input input-bordered input-primary w-full max-w-xs"
                />
                {formik.touched.username && formik.errors.username ? (
                  <p className="text-error">{formik.errors.username}</p>
                ) : null}
              </div>
              <div>
                <label htmlFor="password">Password</label>
                <input
                  name="password"
                  onChange={formik.handleChange}
                  type="password"
                  placeholder="password"
                  className="input input-bordered input-primary w-full max-w-xs"
                />
                {formik.touched.password && formik.errors.password ? (
                  <p className="text-error">{formik.errors.password}</p>
                ) : null}
              </div>
            </div>
            <div>
              <button
                type="submit"
                className={`btn btn-primary btn-outline w-full ${
                  isLoading && "loading"
                }`}
              >
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;
