import { AppDispatch } from "./../index";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { useApi } from "../../hooks/useHttp";

interface userState {
  id: string;
  username: string;
  role: string;
  token: string;
  group_id: string;
}
export interface userResponse extends userState {
  token: string;
  error: string;
}

const initialState: any = {
  data: {},
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    loginUser: (state, action: PayloadAction<any>) => {
      state.data = action.payload;
    },
    logoutUser: (state) => {
      state.data = {};
    },
  },
});

const { loginUser, logoutUser } = userSlice.actions;

export const login = (data: any) => async (dispatch: AppDispatch) => {
  const apiKey = process.env.REACT_APP_API_KEY || "";
  const value = { ...data, apiKey };
  return await useApi()
    .post("/login.php", value)
    .then((resp) => {
      console.log("data: ", resp?.status);
      if (resp?.status == "SUCCESS") {
        dispatch(loginUser(resp?.user));
      }
      return resp;
    });
};

export const logOut = () => (dispatch: AppDispatch) => {
  dispatch(logoutUser());
  return true;
};

export default userSlice.reducer;
