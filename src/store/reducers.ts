import { combineReducers } from "@reduxjs/toolkit";
// slices
import counterReducer from "./counter/counterSlice";
import userSlice from "./user/userSlice";

export const rootReducer = combineReducers({
  counter: counterReducer,
  user: userSlice,
});

export default rootReducer;
