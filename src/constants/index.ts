export enum ROLE {
  TEACHER = "Teacher",
  LIBRARIAN = "Librarian",
  STUDENT = "Student",
}
