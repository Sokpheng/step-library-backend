import { Navigate, Outlet } from "react-router-dom";
import { useAppSelector } from "../hooks";

type Props = {};

function RouteGuard({}: Props) {
  const user = useAppSelector((state) => state.user);
  return user?.data?.role ? <Outlet /> : <Navigate to="/auth" />;
}

export default RouteGuard;
