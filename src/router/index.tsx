import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "../layouts/Layout";
import Home from "../pages";
import NotFoundPage from "../pages/404";
import Login from "../pages/auth";
import LibrarianDashboard from "../pages/librarian/Dashboard";
import BookList from "../pages/BookList";
import Dashboard from "../pages/Dashboard";
import GroupList from "../pages/GroupList";
import StudentInGroup from "../pages/GroupList/StudentInGroup";
import RouteGuard from "./RouteGuard";
import Account from "../pages/Account";
import ManageUsers from "../pages/ManageUsers";
type Props = {};

export default function Router({}: Props) {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/auth" element={<Login />} />
        <Route element={<Layout />}>
          <Route element={<RouteGuard />}>
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/account" element={<Account />} />
            <Route path="/group-list/*">
              <Route index element={<GroupList />} />
              <Route path=":id" element={<StudentInGroup />} />
            </Route>
            <Route path="/manage-users" element={<ManageUsers />} />
            <Route path="/book-list" element={<BookList />} />
          </Route>
        </Route>
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
}
