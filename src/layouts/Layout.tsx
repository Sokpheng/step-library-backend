import { Outlet, useNavigate } from "react-router-dom";
import Navigation, { menuOptions } from "../components/Navigation";
import {
  MdDashboard,
  MdOutlineGroups,
  MdLibraryBooks,
  MdOutlineMenu,
  MdGroups,
} from "react-icons/md";
import { useState, useMemo } from "react";
import { useAppDispatch, useAppSelector } from "../hooks";
import { logOut } from "../store/user/userSlice";
import { toast } from "react-toastify";
import { NavLink } from "react-router-dom";
import { ROLE } from "../constants";
import { FaUserCog } from "react-icons/fa";
import { AiOutlineUsergroupAdd } from "react-icons/ai";

const menuTeacher: menuOptions[] = [
  {
    name: "Dashboard",
    icon: <MdDashboard />,
    path: "/dashboard",
  },
  {
    name: "Group List",
    icon: <MdOutlineGroups />,
    path: "/group-list",
  },
  {
    name: "Book List",
    icon: <MdLibraryBooks />,
    path: "/book-list",
    exceptRole: ROLE.LIBRARIAN,
  },
  {
    name: "Manage Users",
    icon: <FaUserCog />,
    path: "/manage-users",
    exceptRole: ROLE.TEACHER,
  },
];

const Layout = () => {
  const [hideMenu, setHideMenu] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const user = useAppSelector((state) => state.user);

  const handleLogout = () => {
    if (dispatch(logOut())) {
      toast.success("Logout was successful.");
      navigate("/auth");
    }
  };

  return (
    <div className="flex min-h-screen">
      <aside
        className={`flex flex-col bg-gray-100 max-w-xs transition-all ease-linear duration-300 ${
          hideMenu ? "w-0" : "w-auto grow"
        }`}
      >
        <Navigation menus={menuTeacher} />
      </aside>
      <div className="flex flex-col grow">
        <div className="navbar bg-gray-50">
          <div className="flex-1">
            <button
              className="btn btn-ghost"
              onClick={() => setHideMenu(!hideMenu)}
            >
              <MdOutlineMenu className="text-2xl" />
            </button>
          </div>
          <div className="flex-none gap-2">
            <div className="dropdown dropdown-end">
              <div className="flex items-center space-x-2">
                <h2 className="font-semibold capitalize whitespace-nowrap">
                  {user?.data?.username}
                </h2>
                <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
                  <div className="w-10 rounded-full">
                    <img
                      src={user?.data?.image || "static/images/no-profile.png"}
                    />
                  </div>
                </label>
              </div>
              <ul
                tabIndex={1}
                className="mt-3 p-2 shadow menu menu-compact dropdown-content bg-base-100 rounded-box w-52"
              >
                <li>
                  <NavLink to="/account">Profile</NavLink>
                </li>
                <li onClick={handleLogout}>
                  <a>Logout</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <Outlet />
      </div>
    </div>
  );
};

export default Layout;
